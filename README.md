# Table robot 

This code is meant to be flashed into a Dwenguino board 
and it requires the [sensor panel](https://dwengo.org/2019/07/27/robot-control-with-the-sensor-panel/). However, there are only a few lines
that need to be changed if you want to run this in other Arduino 
compatible boards.

The intention is that you learn some tricks by looking at the code.

The robot uses short distance infrared sensors (type Vishay TCRT1000) to detect when the underlying surface is over. 
It probably won't work on dark surfaces or on tables with funny shapes (corners with acute angle).

Enjoy!