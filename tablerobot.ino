#include <LiquidCrystal.h>
#include <Wire.h>
#include <Dwenguino.h>
#include <DwenguinoMotor.h>
#include <DwenguinoSensorPanel.h>

SensorPanel panel;

// Motion parameters and objects
DCMotor MotorL(MOTOR_1_0, MOTOR_1_1);
DCMotor MotorR(MOTOR_2_0, MOTOR_2_1);

#define M_SPEED     140   // Speed of motors
#define BCKW_TIME   600   // Amount of time to move backwards when blocked.
#define MAX_TURNTIME 3000 // Maximum turningtime [ms]

#define SENSE_TIME 20  // Sensor update rate in ms (20 ms == 50 Hz)
#define MIN_STEP 1     // Minimum step for changing paramters

// Seteable parameters: can be set at runtime
unsigned int S_THRSHD = 50;       // Threshold for determinig if a sensor is active (no ground)
unsigned int MIN_TURNTIME = 600;  // Minimum amount of time to rotate when no ground
bool RAND_TURNTIME = false;       // whether turntime is random. If it is a "?" is shown next to the turntime

// A.I. variables
typedef enum GroundState : unsigned char {front='F', right='R', left='L', behind='B'} HasGround; 

HasGround groundis = front, groundwas = front; // Current and previous sensor state
unsigned long int started_turning = 0;         // Time when the robot started turning (ms)
unsigned long int dt_turning = 0;              // Time elapsed since the robot started turning (ms)
unsigned long int last_statechange = 0;        // Time when the last time the sensor state changed (ms)
unsigned long int dt_statechange = 0;          // Time elapsed since the robot changed states (ms)
unsigned long counter = 0;                     // Counting how many times the robot bounced from one side to the other 
unsigned int turntime = 0;                     // Actual time used to turn (pseudo random)

// Working variables
unsigned long int last_sensing = 0;      // Time when we updated the sensor values (ms)
unsigned int sensorL, sensorR;           // Ground sensors value

// Other variables
const char clcl[] = "                 "; // Clear a line of the LCD
bool move_forward = true;                // flag to indicate motion forward

// Helping functions
void dontFall();     // Executed when the robot senses no ground
void readParams();   // To update MIN_TURNTIME and S_THRSHD
void updateState();  // To update the sensor state
void printStateChange();    // Show information on LCD
void printMoveForward();    // Show information on LCD
void moveStraight(const int s); // To move with both motor at same speed

void setup() {
  initDwenguino();
  panel = SensorPanel();
  panel.init();
  
  dwenguinoLCD.clear();
  dwenguinoLCD.print("Set & press C");
  
  // Read values from buttons S, and N, until C is pressed
  while (digitalRead(SW_C) != PRESSED) readParams();
  MotorL.setSpeed(M_SPEED);
  MotorR.setSpeed(M_SPEED);
}

void loop() {
  // Measures every SENSE_TIME has passed
  // Update state and act
  if ((millis() - last_sensing) > SENSE_TIME)
  {
    // Update sensor values
    sensorL = panel.readSensor(OS4,DIFF_MODE);
    sensorR = panel.readSensor(OS1,DIFF_MODE);

    groundwas = groundis;       // Store previous state
    updateState();              // Update current state

    dt_turning = millis() - started_turning; // Compute time since it called dontFall
    dt_statechange = millis() - last_statechange; // Compute time since last state change
    
    // ** Act based on current state **
    // Do things if the state changed
    if (groundis != groundwas)
    {
      last_statechange = millis();  // update time of state change

      // Increase counter is state swtiches fall within specified time interval
      if (dt_statechange > 2*SENSE_TIME && dt_statechange < 350)
        counter++;
      // If we bounced too many times move backwards slowly
      // This action blocks any other
      if (counter > 10)
      {
        moveStraight(-100);
        counter = 0;
        delay(BCKW_TIME);
      }

      // there was no ground, turn!
      if (groundis != front)
      {
        started_turning = millis();
        dontFall();
        // Decide amount of time turning
        turntime = MIN_TURNTIME;
        if (RAND_TURNTIME) turntime += ((micros() % 1001) / 1000.0) * (MAX_TURNTIME - MIN_TURNTIME);
      }

      // Indicate that it can move forward
      if (groundis == front) move_forward = true;
      
      printStateChange();
    }

    // Moves forward only if indicated and enough time has passed since we started turning
    if (groundis == front && move_forward && dt_turning > turntime)
    {
      moveStraight(M_SPEED); 
      printMoveForward();
      move_forward = false;
    }

    // ** end Act **
  } // end sensing
}

void updateState()
{
  panel.setHeadlights(LD2,false);
  panel.setHeadlights(LD1,false);
  if (sensorL > S_THRSHD && sensorR > S_THRSHD) groundis = front; // Ground is detected on both sides
  if (sensorL < S_THRSHD)
  {
    groundis = right; // Ground missing on the left
    panel.setHeadlights(LD2,true);
  }
  if (sensorR < S_THRSHD)
  {
    groundis = left; // Ground missing on the right
    panel.setHeadlights(LD1,true);
  }
  if (sensorR < S_THRSHD && sensorL < S_THRSHD) groundis = behind; // Ground missing on both sides
}

void moveStraight(const int vel)
{
  MotorL.setSpeed(vel);
  MotorR.setSpeed(vel);
}

void dontFall ()
{
    short int l = 1, r = -1;

    if (groundis == right) {l = 1; r = -1;}      // turn right
    else if (groundis == left) {l = -1; r = 1;}  // turn left
    else if (groundis == behind) {               // turn random
      l = 2 * (micros()%2) - 1; r = -l;
    } 
  
    // Turn around in place
    MotorL.setSpeed(l*M_SPEED);
    MotorR.setSpeed(r*M_SPEED);
}

void read_button (const int B, unsigned int & value, const int dir, const unsigned int limitV)
{
  unsigned long whenPressed = 0;
  unsigned long dt;
  const int mindv = dir * MIN_STEP;
  int dv = mindv;
  
  if (digitalRead(B) == PRESSED) whenPressed = millis();
  while (digitalRead(B) == PRESSED)
  {
    dt = millis() - whenPressed;
    // Scale step base don time pressed
    if (dt > 7000)      dv = 1000 * mindv;    
    else if (dt > 4000) dv = 100 * mindv;    
    else if (dt > 1000) dv = 10  * mindv;
    else                dv = mindv;

    // Limit value
    if (dir > 0 && value >= limitV)
      value = limitV;
    else if (dir < 0 && (value <= limitV || value <= -dv)) // Check that negative increments do not overflow
    {
      value = limitV;
      RAND_TURNTIME = !RAND_TURNTIME; // Switch between random and not random
    }
    else
      value += dv;

    dwenguinoLCD.setCursor(0,1);
    dwenguinoLCD.print(clcl);
    dwenguinoLCD.setCursor(0,1);
    dwenguinoLCD.print(value);
    delay(200);
   }
}

void readParams()
{
  read_button (SW_S, MIN_TURNTIME, -1, 2 * SENSE_TIME); // min turntime is 2 x SENSE_TIME
  read_button (SW_N, MIN_TURNTIME, 1, MAX_TURNTIME);    // max turntime is MAX_TURNTIME
  read_button (SW_W, S_THRSHD, -1, 0);                  // min threshold is 0
  read_button (SW_E, S_THRSHD, 1, 1023);                // max threshold is max output from sensor
  
  dwenguinoLCD.setCursor(0,1);
  dwenguinoLCD.print(MIN_TURNTIME);
  if (RAND_TURNTIME) dwenguinoLCD.print("?");
  dwenguinoLCD.print(" ms ");
  dwenguinoLCD.print(S_THRSHD);
  LEDS = 0;
}

void printStateChange()
{
  dwenguinoLCD.setCursor(0,0);
  dwenguinoLCD.print(clcl);  
  dwenguinoLCD.setCursor(0,0);
  dwenguinoLCD.print(static_cast<char>(groundwas));
  dwenguinoLCD.print(">");  
  dwenguinoLCD.print(static_cast<char>(groundis));
  dwenguinoLCD.print(",");  
  dwenguinoLCD.print(counter);
  dwenguinoLCD.print("/");  
  dwenguinoLCD.print(sensorL);
  dwenguinoLCD.print(",");  
  dwenguinoLCD.print(sensorR);
  dwenguinoLCD.setCursor(0,1);
  dwenguinoLCD.print(clcl);  
  dwenguinoLCD.setCursor(0,1);
  dwenguinoLCD.print(turntime);
  LEDS = 0;
}
void printMoveForward()
{
  dwenguinoLCD.setCursor(0,1);
  dwenguinoLCD.print(clcl);  
  dwenguinoLCD.setCursor(0,1);
  dwenguinoLCD.print(turntime);
  dwenguinoLCD.print(" <= ");  
  dwenguinoLCD.print(dt_turning);
  LEDS = 0;
}
